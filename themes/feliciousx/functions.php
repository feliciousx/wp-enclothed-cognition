<?php

if ( ! function_exists( 'feliciousx_support' ) ) :
	function feliciousx_support()  {

		// Adding support for core block visual styles.
		add_theme_support( 'wp-block-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style.css' );
	}
	add_action( 'after_setup_theme', 'feliciousx_support' );
endif;

/**
 * Enqueue scripts and styles.
 */
function feliciousx_scripts() {
	// Enqueue theme stylesheet.
	wp_enqueue_style( 'reset-style', get_template_directory_uri() . '/assets/css/reset.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'feliciousx-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );
}

add_action( 'wp_enqueue_scripts', 'feliciousx_scripts' );
