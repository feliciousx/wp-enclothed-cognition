# wp-enclothed-cognition

Wordpress Theme for Enclothed Cognition

## TODOs

- [ ] Header Template
    - Nav is affected by Pages
    - Soc Med links
    - How does Search work? remove for now maybe
    - Title, tagline, photo?
- [ ] Footer Template
    - copyright
    - links to soc med and contact
- [ ] Page Template
    - Side Panel showing About Me
    - Side Panel showing Ig feed
    - Recent Posts?
- [ ] Home Page
    - List of posts
- [ ] About Page
- [ ] Contact
- [ ] Tags